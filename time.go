package vacation

import "time"

func ParseTimePanic(format string, dateStr string) time.Time {
	t, err := time.Parse(format, dateStr)
	PanicW("Parsing contract.valid_from", err)
	return t
}

func ParseTimeOrDefault(format string, dateStr string, defaultDate time.Time) time.Time {
	if dateStr != "" {
		return ParseTimePanic(format, dateStr)
	}
	return defaultDate
}

func ParseTimeOrNow(format string, dateStr string) time.Time {
	return ParseTimeOrDefault(format, dateStr, time.Now())
}

func ParseTimeOrFuture(format string, dateStr string) time.Time {
	return ParseTimeOrDefault(format, dateStr, time.Date(3000, 1, 1, 0, 0, 0, 0, time.UTC))
}
