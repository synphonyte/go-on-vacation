package vacation

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

// LogRequest indicate to log all Resquests
var LogRequest bool

// LogResponse indicate to log all Responses
var LogResponse bool

// Client ...
type Client struct {
	baseURL string
}

// NewClient ...
func NewClient(url string) *Client {
	return &Client{
		baseURL: url,
	}
}

// POST ...
func (c *Client) POST(payload []byte, soapAction string) (*[]byte, error) {
	req, err := http.NewRequest("POST", c.baseURL, bytes.NewReader(payload))
	if err != nil {
		return nil, err
	}

	setHeaders(req, soapAction)

	if LogRequest {
		log.Println("--SOAP request: ", string(payload))
	}
	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer func() {
		cerr := resp.Body.Close()
		if err == nil {
			err = cerr
		}
	}()

	bytesBody, err := readResponse(resp)
	if err != nil {
		return nil, err
	}
	if LogResponse {
		log.Println("--SOAP response : ", string(*bytesBody))
	}

	return bytesBody, nil
}

func setHeaders(req *http.Request, soapAction string) {
	req.Header.Set("Content-type", "text/xml; charset=UTF-8")
	req.Header.Set("Accept-Encoding", "gzip, deflate")
	if len(soapAction) > 0 {
		req.Header.Set("SOAPAction", "https://tempuri.org/"+soapAction)
	}
}

func readResponse(resp *http.Response) (*[]byte, error) {
	// Check that the server actual sent compressed data
	var reader io.ReadCloser
	var err error
	switch resp.Header.Get("Content-Encoding") {
	case "gzip":
		reader, err = gzip.NewReader(resp.Body)

		if err != nil {
			return nil, err
		}

		defer func() {
			cerr := reader.Close()
			if err == nil {
				err = cerr
			}
		}()
	default:
		reader = resp.Body
	}

	byteBody, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusOK {

		return &byteBody, nil
	}
	return nil, fmt.Errorf("HTTP Code %d. %s", resp.StatusCode, string(byteBody))
}
