package vacation

import "log"

func TestConvertXmlDecimalToInt64() {
	log.Println(ConvertXmlDecimalToInt64("123,123", ",", "."))
	log.Println(ConvertXmlDecimalToInt64("4.123,126", ",", "."))
	log.Println(ConvertXmlDecimalToInt64("1,6666666666666", ",", "."))
	log.Println(ConvertXmlDecimalToInt64("456,7", ",", "."))
	log.Println(ConvertXmlDecimalToInt64("789", ",", "."))
	log.Println(ConvertXmlDecimalToInt64("7.89", ".", " "))
}
