package vacation

import (
	"fmt"
	"log"
)

func NewEmptyError(source string) error {
	return fmt.Errorf("empty response from %s", source)
}

func Panic(err error) {
	if err != nil {
		log.Println("Error running: ", err.Error())
		panic(err)
	}
}

func PanicW(where string, err error) {
	if err != nil {
		log.Println("Error running: ", where, ": ", err.Error())
		panic(err)
	}
}

func NoPanic(err error) {
	if err != nil {
		log.Println("Error running: ", err.Error())
	}
}

func NoPanicW(where string, err error) {
	if err != nil {
		log.Println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
		log.Println("Error running: ", where, ": ", err.Error())
		log.Println("¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡")
	}
}

func DieIf(err error) {
	if err != nil {
		log.Print("Abnormal exit: ", err.Error())
	}
}

func ReturnByErrorW(where string, err error) bool {
	NoPanicW(where, err)
	return err != nil
}
