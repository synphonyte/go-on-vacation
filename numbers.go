package vacation

import (
	"strconv"
	"strings"
)

func ConvertXmlDecimalToInt64(amount string, decimalSeparator string, thousandsSeparator string) int64 {
	if amount == "" {
		return 0
	}

	parts := strings.Split(
		strings.ReplaceAll(amount, thousandsSeparator, ""),
		decimalSeparator,
	)

	parsedValue, err := strconv.ParseInt(parts[0], 10, 64)
	parsedValue *= 1000

	if err != nil {
		panic(err)
	}

	if len(parts) > 1 {

		secondPart := parts[1] + "0000"

		if secondPart[3] >= '5' {
			parsedValue += 1
		}

		secondPart = secondPart[:3]

		decimals, err := strconv.ParseInt(secondPart, 10, 64)

		if err != nil {
			panic(err)
		}

		parsedValue += decimals
	}

	return parsedValue
}
